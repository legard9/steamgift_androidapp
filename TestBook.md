# Test Book for System Testing

**1-lista-giveaway**

- Visualizzazione lista giveaway (no login)
    + Entrare nell'app e nel sito senza loggarsi
    + Verificare che i giveaway visualizzati nella lista corrispondano a quelli del sito

- Visualizzazione lista giveaway (login)
    + Entrare nell'app e nel sito loggandosi
    + Verificare che i giveaway visualizzati nella lista corrispondano a quelli del sito

- Refresh lista 
    + Entrare nell'app
    + Aspettare almeno un minuto
    + Eseguire un pull to refresh 
    + Verificare che il tempo rimanente in ogni giveaway sia diminuito

- Refresh lista (giveaway scaduto)
    + Entrare nell'app
    + Aspettare che il tempo per accedere ad almeno un giveaway scada
    + Eseguire un pull to refresh
    + Verificare che il giveaway scaduto scompaia dalla lista

- Aggiornamento lista
    + Entrare nell'app e nel sito con le medesime credenziali
    + Aspettare almeno un minuto
    + Entrare nella pagina dedicata di qualsiasi giveaway
    + Tornare alla pagina precedente tramite il tasto "indietro" del dispositivo
    + Verificare che il tempo rimanente in ogni giveaway sia diminuito
    + Verificare che la lista corrisponda con quel del sito

- Aggiornamento lista (giveaway scaduto)
    + Entrare nell'app
    + Aspettare che il tempo per accedere ad almeno un giveaway scada
    + Entrare nella pagina dedicata di qualsiasi giveaway
    + Tornare alla pagina precedente tramite il tasto "indietro" del dispositivo
    + Verificare che il giveaway scaduto scompaia dalla lista

- Caricamento ulteriori giveaways
    + Entrare nell'app
    + Scrollare verso il basso fino a che permesso
    + Verificare che dopo un breve caricamento si carichino ulteriori giveaway nella lista principali

**2-scheda-giveaway**

- Visualizzazione scheda giveaway (no login)
    + Entrare nell'app e nel sito
    + Selezionare dalla lista lo stesso giveaway sul dispositivo e sul sito
    + Verificare che le informazioni mostrare su app corrispondano a quelle del sito

- Visualizzazione scheda giveaway (login)
    + Entrare nell'app e nel sito
    + Loggarsi su entrambe le piattaforme con le stesse credenziali
    + Selezionare dalla lista lo stesso giveaway sul dispositivo e sul sito
    + Verificare che le informazioni mostrare su app corrispondano a quelle del sito

**3-login**

- Login
    + Premere sul pulsante login nel drawer
    + Verificare che sia visualizzata la pagina di login di Steam.com
    + Loggarsi con le proprie credenziali
    + Verificare che una volta terminata la procedura l'app abbia aggiornato lo stato dell'utente
    +  Verificare che le informazioni dell'utente siano corrette paragonandole con quelle del sito

**4-partecipazione-giveaway**

- Partecipazione a un Giveaway dall' APP (Successo)
    + Login nell'app e nel sito con le medesime credenziali
    + Dall' app selezionare un giveaway in cui non si é ancora partecipato
    + Partecipare al giveaway tramite il bottone verde
    + Verificare che il bottone cambi colore e testo
    + Dal sito selezionare il medesimo giveaway e verificare che il sito segnali che l'utente sta partecipando

- Partecipazione a un Giveaway dall' APP (Fallimento di rete)
    + Login nell'app
    + Dall' app selezionare un giveaway in cui non si é ancora partecipato
    + Inserire la modalitá Aereo nel dispositivo
    + Partecipare al giveaway tramite il bottone verde
    + Verificare che l'app segnali l'errore durante l'operazione

- Partecipazione a un Giveaway dall' APP (Fallimento per punti/livello)
    + Login nell'app
    + Dall' app selezionare un giveaway in cui non si é ancora partecipato con costo superiore ai punti posseduti o di livello superiore a quello dell'utente
    + Partecipare al giveaway tramite il bottone grigio
    + Verificare che l'app segnali l'errore corrispondente durante l'operazione

- Partecipazione a un Giveaway dal sito (Successo)
    + Login nell'app e nel sito con le medesime credenziali
    + Dal sito selezionare un giveaway in cui non si é ancora partecipato
    + Partecipare al giveaway dal sito
    + Selezionare il medesimo giveaway dall'app
    + Verificare che il pulsante in basso segnali che si é giá acceduti nel giveaway

- Uscita da un Giveaway dall'APP (Successo)
    + Login nell'app e nel sito con le medesime credenziali
    + Dall' app selezionare un giveaway in cui si sta giá partecipando
    + Uscire dal giveaway tramite il bottone rosso
    + Verificare che il bottone cambi colore e testo
    + Dal sito selezionare il medesimo giveaway e verificare che il sito segnali che l'utente é uscito dal giveaway

- Uscita da un Giveaway dall' APP (Fallimento di rete)
    + Login nell'app
    + Dall' app selezionare un giveaway in cui si sta giá partecipando
    + Inserire la modalitá Aereo nel dispositivo
    + Uscire dal giveaway tramite il bottone rosso
    + Verificare che l'app segnali l'errore durante l'operazione

- Uscita da un Giveaway dal sito (Successo)
    + Login nell'app e nel sito con le medesime credenziali
    + Dal sito selezionare un giveaway in cui si sta giá partecipando
    + Uscire dal giveaway dal sito
    + Selezionare il medesimo giveaway dall'app
    + Verificare che il pulsante in basso segnali che si é fuori dal giveaway

**5-burger-menu**

- Funzionamento generale (utente non loggato)
    + Dall'app aprire il burger menu
    + Verificare che sia presente il tasto per fare login se l'utente non è loggato
    + Verificare che il bottone di login faccia partire la procedura di login tramite webview

- Funzionamento generale (utente loggato)
    + Dall'app aprire il burger menu
    + Verificare che sia presente il tasto per visualizzare i giveaway a cui si è participato, il profilo personale, la blacklist e il bottone logout
    + Verificare che il bottone di logout faccia partire la procedura di logout tramite webview
    + Verificare che il bottone "partecipazione" porti alla pagina dei giveaway partecipati
    + Verificare che il bottone "profilo personale" porti alla pagina del profilo personale
    + Verificare che il bottone "blacklist" porti alla pagina con la blacklist utenti

- Utente non loggato nel burger menu
    + Verificare che se la procedura di login non é portata a termine correttamente o l'app é appena stata aperta non ci siano informazioni sull'utente in cima al burger menu

- Login dal burger menu
    + Loggarsi nell' app tramite il burger menu e nel sito con le medesime credenziali
    + Verificare che una volta loggati le informazioni presenti in cima al burger menu corrispondano con quelle nel sito

**7-visualizzazione-utente-loggato**

- Confronto sito-App
    + Login nell'app e nel sito con le medesime credenziali
    + Controllare dal burger menu e dal sito che tutte le informazioni e dati mostrati combacino

- Partecipazione a un Giveaway dall' APP
    + Login nell'app e nel sito con le medesime credenziali
    + Controllare dal burger menu e dal sito i punti disponibili
    + Dall' app selezionare un giveaway in cui non si é ancora partecipato
    + Partecipare al giveaway tramite il bottone verde
    + Entrare nel burger menu e verificare che i punti disponibili siano diminuiti correttamente
    + Dal sito controllare che i punti disponibili combacino con quelli dell'app

- Partecipazione a un Giveaway dal sito
    + Login nell'app e nel sito con le medesime credenziali
    + Controllare dal burger menu e dal sito i punti disponibili
    + Dal sito selezionare un giveaway in cui non si é ancora partecipato
    + Partecipare al giveaway
    + Eseguire refresh della applicazione tramite pull verso il basso
    + Entrare nel burger menu e verificare che i punti disponibili combacino con quelli dell'app

- Uscita da un Giveaway dall' APP
    + Login nell'app e nel sito con le medesime credenziali
    + Controllare dal burger menu e dal sito i punti disponibili
    + Dall' app selezionare un giveaway in cui si sta giá partecipando
    + Uscire dal giveaway tramite il bottone rosso
    + Entrare nel burger menu e verificare che i punti disponibili siano aumentati correttamente
    + Dal sito controllare che i punti disponibili combacino con quelli dell'app

- Uscita da un Giveaway dal sito
    + Login nell'app e nel sito con le medesime credenziali
    + Controllare dal burger menu e dal sito i punti disponibili
    + Dal sito selezionare un giveaway in cui si sta giá partecipando
    + Uscire dal giveaway
    + Eseguire refresh della applicazione tramite pull verso il basso
    + Entrare nel burger menu e verificare che i punti disponibili combacino con quelli dell'app
    

**8-blacklist-utenti**

- Visualizzazione blacklist
    + Loggarsi sull'applicazione
    + Accedere alla pagina della blacklist
    + Verificare che la blacklist combaci con quella del sito

- Visualizazione blacklist (vuota)
    + Loggarsi sull'applicazione
    + Accedere alla pagina della blacklist
    + Verificare che venga visualizzata correttamente la textview informativa

- Visualizzazione blacklist (errore connessione)
    + Loggarsi sull'applicazione
    + Disattivare la connessione a internet
    + Accedere alla pagina della blacklist
    + Verificare che venga visualizzata una snackbar di errore

- Aggiunta di un utente dalla blacklist da pagina dettagli giveaway
    + Loggarsi nell'applicazione
    + Accedere alla pagina dettagli di un qualsiasi giveaway
    + Premere il bottone per bloccare l'utente
    + Assicurarsi che il testo del bottone sia cambiato
    + Verificare sul sito che la blacklist sia stata aggiornata

- Aggiunta di un utente dalla blacklist da pagina dettagli giveaway (errore di connessione)
    + Loggarsi nell'applicazione
    + Accedere alla pagina dettagli di un qualsiasi giveaway
    + Disattivare la connessione a internet
    + Premere il bottone per bloccare l'utente
    + Verificare che venga visualizzata la snackbar di errore

- Rimozione di un utente dalla blacklist da pagina dettagli giveaway
    + Loggarsi nell'applicazione
    + Accedere alla pagina dettagli di un qualsiasi giveaway
    + Premere il bottone per bloccare l'utente
    + Assicurarsi che il testo del bottone sia cambiato
    + Verificare sul sito che la blacklist sia stata aggiornata
    + Premere sul bottone per sbloccare l'utente
    + Verificare sul sito che la blacklist sia stata aggiornata

- Rimozione di un utente dalla blacklist da pagina dettagli giveaway (errore connessione)
    + Loggarsi nell'applicazione
    + Accedere alla pagina dettagli di un qualsiasi giveaway
    + Disattivare la connessione a internet
    + Premere il bottone per sbloccare l'utente
    + Verificare che venga visualizzata una snackbar di errore

- Rimozione di un utente dalla blacklist da pagina blacklist
    + Loggarsi nell'applicazione
    + Accedere alla pagina della blacklist
    + Verificare che la lista combaci con quella presente sul sito
    + Disattivare la connessione a internet
    + Premere sul bottone "rimuovi" per rimuovere l'utente dalla blacklist

- Rimozione di un utente dalla blacklist da pagina blacklist
    + Loggarsi nell'applicazione
    + Accedere alla pagina della blacklist
    + Verificare che la lista combaci con quella presente sul sito
    + Premere sul bottone "rimuovi" per rimuovere l'utente dalla blacklist
    + Confermare l'azione sul dialog
    + Verificare che l'utente sparisca dalla lista
    + Verificare che l'utente sia rimosso anche dalla blacklist del sito

**9-logout**

- Visualizzazione utente
    + Login nell'app tramite il burger menu
    + Verificare che il login sia avvenuto con successo
    + Verificare che il bottone login sia sparito
    + Verificare che il bottone logout sia visibile come ultima opzione
    + Premere l'opzione logout
    + Verificare che si chiuda il drawer e quindi riaprirlo
    + Verificare che il drawer sia tornato nella visualizzazione default come utente non loggato
    + Verificare che l'opzione di login sia l'unica visibile
    
- Divieto di partecipazione ai giveaway
    + Login nell'app tramite il burger menu
    + Verificare che il login sia avvenuto con successo
    + Eseguire il logout
    + Accedere ai dettagli di un qualsiasi giveaway
    + Verificare che il bottone di partecipazione non sia presente

**10-pagina-profilo**

- Visualizzazione Statistiche
    + Login dall'app e dal sito con le medesime credenziali
    + Entrare nella sezione profilo utente dall'app e dal sito 
    + Verificare che le informazioni corrispondano

- Visualizzazione Statistiche modificate
    + Login dall'app e dal sito con le medesime credenziali
    + Dal sito partecipare a un giveaway
    + Entrare nella pagina del profilo dall'app
    + Verificare che le statistiche di Giveaway entrati e punti si siano aggiornate correttamente

**11-gestione-sessione-login**

- Permanenza login
    + Effettuare login nell'app
    + Uscire dalla applicazione
    + Chiudere applicazione tramite task manager Android
    + Entrare nell'applicazione
    + Verificare che l'accesso sia stato mantenuto accedendo a un giveaway oppure nell'header del burger menu

- Permanenza logout
    + Effettuare login nell'app
    + Effettuare logout dall'app
    + Uscire dalla applicazione
    + Chiudere applicazione tramite task manager Android
    + Entrare nell'applicazione
    + Verificare che l'accesso non sia stato mantenuto visualizzando l'header del burger menu e provando ad accedere a un giveaway

**13-lista-giveaway-partecipati**

- Visualizzazione giveaway partecipati (prima pagina)
    + Effettuare login dall'app e dal sito con le medesime credenziali
    + Entrare dall'app nella sezione partecipati tramite il burger menu
    + Entrare nella sezione partecipati dal sito
    + Verificare che i giveaway visualizzati nella prima pagina del sito siano presenti nella sezione dell'app

- Visualizzazione giveaway partecipati (successive pagine)
    + Effettuare login dall'app e dal sito con le medesime credenziali
    + Entrare dall'app nella sezione partecipati tramite il burger menu
    + Entrare nella sezione partecipati dal sito
    + Dal sito passare alla pagina successiva dei giveaway partecipati
    + Dall'app scrollare verso il basso fino in fondo
    + Verificare che dopo un breve caricamento vengano visualizzati i giveaway presenti alla pagina del sito
    + Ripetere il procedimento