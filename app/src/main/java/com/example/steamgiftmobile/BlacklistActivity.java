package com.example.steamgiftmobile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.steamgiftmobile.adapter.BlacklistAdapter;
import com.example.steamgiftmobile.adapter.DividerItemDecoration;
import com.example.steamgiftmobile.classes.User.GenericUser;
import com.example.steamgiftmobile.http.callbacks.BlacklistCallback;

import com.example.steamgiftmobile.http.HTTPClient;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;


public class BlacklistActivity extends AppCompatActivity {
    private BlacklistAdapter blacklistAdapter;
    private TextView emptyText;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blacklist);

        blacklistAdapter = new BlacklistAdapter(this);
        recyclerView = findViewById(R.id.recyclerView);

        //Add line divisor between each element
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this));

        recyclerView.setAdapter(blacklistAdapter);

        loadBlacklist();
    }

    private void loadBlacklist(){
        //loading of the blacklist from website
        HTTPClient.INSTANCE.loadBlacklist( new BlacklistCallback() {
            //list empty
            @Override
            public void onEmpty() {
                runOnUiThread(() ->{
                    emptyText = findViewById(R.id.textEmpty);
                    emptyText.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.INVISIBLE);
                });
            }
            //some users in blacklist
            @Override
            public void onSuccess(ArrayList<GenericUser> users) {
                runOnUiThread(() -> {
                    blacklistAdapter.submitList(users);
                    blacklistAdapter.notifyDataSetChanged();
                });
            }
            //error during retrieval of blacklist
            @Override
            public void onError() {
                runOnUiThread(() -> showErrorSnackbar());
            }
        });
    }

    private void showErrorSnackbar() {
        View rootView = findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(rootView,
                "Errore durante il caricamento della blacklist", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Riprova", v -> {
            loadBlacklist();
            snackbar.dismiss();
        });
        snackbar.show();
    }
}