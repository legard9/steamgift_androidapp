package com.example.steamgiftmobile;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.steamgiftmobile.classes.Session;

public class LoginActivity extends AppCompatActivity {

    private static final String LOGIN_URL ="https://www.steamgifts.com/?login";
    private static final String REDIRECT_URL = "https://www.steamgifts.com/";
    private static final String STEAM_OPENID_URL = "https://steamcommunity.com/openid/";

    private WebView webView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        webView = findViewById(R.id.webView);
        progressBar = findViewById(R.id.progressBar);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webView.setWebViewClient(new SteamWebViewClient());

        webView.addJavascriptInterface(new JavaScriptContentHandler(), "contenthandler");
        webView.loadUrl(LOGIN_URL);
    }

    // When login has successful outcome
    private void onLoginSuccessful(String sessionID){
        MainActivity.currentUser.setSessionID(sessionID);

        Session session = new Session(this);
        session.setString("sessionID",sessionID);

        //Use the HTML to extract user info (see JavaScriptContentHandler at eof)
        webView.loadUrl("javascript:contenthandler.processHTML(document.documentElement.outerHTML);");
    }

    // When login has unsuccessful outcome and can't extract proper info
    private void onLoginCancelled(){
        finish();
    }

    private class SteamWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Log.d("Steam", "Page started: " + url);

            //Checking what page we are loading to decide which widget to show
            if (REDIRECT_URL.equals(url)) {
                webView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            }

            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (REDIRECT_URL.equals(url)) {
                // Fetch all cookies
                String cookie = CookieManager.getInstance().getCookie(url);
                Log.v("Fatto", "Cookies: " + cookie);

                // Look for the session id
                for (String c : cookie.split("; ")) {
                    String[] details = c.split("=", 2);
                    if ("PHPSESSID".equals(details[0])) {
                        Log.d("Fatto", "onLoginSuccessful(" + details[1].trim() + ")");
                        onLoginSuccessful(details[1].trim());
                        return;
                    }
                }

                Log.d("Non fatto", "onLoginCancelled()");
                onLoginCancelled();
            } else if (url.startsWith(STEAM_OPENID_URL)) {
                //As onPageStarted. Just checking url and set widgets visibility
                // Actual steam login, show the webview
                webView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    //We use this class to manipulate and use the application methods/class inside the webview
    private class JavaScriptContentHandler {
        @JavascriptInterface
        public void processHTML(String html) {

            MainActivity.currentUser.extractFromHTML(html);

            finish();
        }
    }
}
