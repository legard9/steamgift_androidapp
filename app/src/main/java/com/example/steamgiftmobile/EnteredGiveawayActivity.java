package com.example.steamgiftmobile;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.steamgiftmobile.adapter.DividerItemDecoration;
import com.example.steamgiftmobile.adapter.MainAdapter;
import com.example.steamgiftmobile.classes.Giveaway;
import com.example.steamgiftmobile.http.HTTPClient;
import com.example.steamgiftmobile.http.callbacks.GiveawayListCallback;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

public class EnteredGiveawayActivity extends AppCompatActivity {

    private int pageCount;
    private int lastPage = 0;
    private List<Giveaway> currentGiveAwayList;
    private MainAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enteredgiveaway);

        //Creating the adapter to show giveaway data
        adapter = new MainAdapter(this);

        pageCount = 1;

        RecyclerView recyclerView = findViewById(R.id.recyclerViewEntered);

        //Add line divisor between each element
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this));

        recyclerView.setAdapter(adapter);

        loadData(pageCount);
    }

    public int getPageCount(){return pageCount;}
    public int getLastPage(){return lastPage;}

    public void loadData(){loadData(1);}

    public void loadData(int page){
        HTTPClient.INSTANCE.loadEnteredGiveaways(page, new GiveawayListCallback() {
            @Override
            public void onSuccess(List<Giveaway> giveawayList, int last) {
                runOnUiThread(() -> {
                    lastPage = last;
                    //when called by onCreate loads the first page only and reset the pagecount
                    if(page == 1){
                        currentGiveAwayList = giveawayList;
                    }else{
                        //pagecount is > 1 so append the old list with the next page and increment
                        pageCount++;

                        // Null item removal
                        currentGiveAwayList.remove(currentGiveAwayList.size() - 1);
                        //Append the new list to the older one
                        currentGiveAwayList.addAll(giveawayList);
                    }

                    if(pageCount < lastPage) currentGiveAwayList.add(null);

                    //submit new currentList to the adapter and notify the change
                    adapter.submitList(currentGiveAwayList);
                    adapter.notifyDataSetChanged();
                });
            }

            @Override
            public void onError() {
                runOnUiThread(() -> showErrorSnackbar());
            }
        });
    }

    private void showErrorSnackbar() {
        View rootView = findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(rootView,
                "Errore durante il caricamento dei giveaway", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Riprova", v -> {
            loadData(pageCount + 1);
            snackbar.dismiss();
        });
        snackbar.show();
    }
}
