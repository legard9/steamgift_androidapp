package com.example.steamgiftmobile.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.steamgiftmobile.R;
import com.example.steamgiftmobile.classes.User.GenericUser;
import com.example.steamgiftmobile.http.HTTPClient;
import com.example.steamgiftmobile.http.callbacks.PostCallback;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

// This class is used by recyclerView to adapt the items we want to show in the view
public class BlacklistAdapter extends ListAdapter<GenericUser,BlacklistAdapter.CustomViewHolder>{
    private Activity context;

    public BlacklistAdapter(Activity context) {
        // Call to the diff checker (see end of file)
        super(DIFF_CALLBACK);
        this.context = context;
    }

    // ViewHolder is used to construct and recycle the various items we will pass and is
    // executed for each element visible on the screen (for example 8 object in the list ->
    // executed 8 times)
    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Go get the xml layout and inflate to create the view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.item_blacklist, parent, false);
        // We pass the clickListener too to be able to click the view
        return new MainViewHolder(view);

    }

    //This method is used to render the object in the item list
    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {

        //Bind the ViewHolder to the item we want to render
        ((MainViewHolder) holder).bindTo(getItem(position));

        ((MainViewHolder) holder).blacklistButton.setOnClickListener(v -> {

            GenericUser user = ((MainViewHolder) holder).user;

            //build the dialog for confirmation
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setTitle("Rimuovere " + user.getUsername() + " dalla blacklist?");
            builder.setMessage("Potrai rivedere i giveaway creati da questo utente");
            builder.setPositiveButton("Conferma", (dialog, id) -> {
                //remove user from blacklist
                HTTPClient.INSTANCE.postBlacklist(user.getId(), "delete", new PostCallback() {
                    @Override
                    public void onSuccess(String response) {
                        context.runOnUiThread(() -> {
                            //submit updated list to adapter
                            ArrayList<GenericUser> list = new ArrayList<GenericUser>(getCurrentList());
                            list.remove(user);
                            submitList(list);

                            //no more users in list
                            if(list.size()==0){
                                TextView emptyText = context.findViewById(R.id.textEmpty);
                                emptyText.setVisibility(View.VISIBLE);
                            }

                            dialog.dismiss();
                        });
                    }

                    @Override
                    public void onError() {
                        context.runOnUiThread(() -> {
                            dialog.dismiss();
                            View rootView = context.findViewById(android.R.id.content);
                            Snackbar snackbar = Snackbar.make(rootView,
                                    "Errore durante la rimozione", Snackbar.LENGTH_INDEFINITE);
                            snackbar.setAction("Riprova", view -> snackbar.dismiss());
                            snackbar.show();
                        });
                    }
                });
            });

            builder.setNegativeButton("Annulla", (dialog, id) -> dialog.dismiss());
            AlertDialog alert = builder.create();
            alert.show();
        });
    }

    // Custom class based on ViewHolder
    static class CustomViewHolder extends RecyclerView.ViewHolder{
        public CustomViewHolder(View itemView) {
            super(itemView);
        }
    }

    //MainViewHolder is the viewHolder we use to display data items
    static class MainViewHolder extends CustomViewHolder {
        private TextView userText;
        private ImageView avatarImage;
        private Button blacklistButton;
        private GenericUser user;

        MainViewHolder(@NonNull View itemView) {
            super(itemView);
            userText = itemView.findViewById(R.id.textUser);
            avatarImage = itemView.findViewById(R.id.avatarImage);
            blacklistButton = itemView.findViewById(R.id.blacklist_button);
        }

        //bind the User object to the fields
        void bindTo(GenericUser user) {
            this.user = user;
            userText.setText(user.getUsername());
            if(user.getAvatarURL() != null){
                Glide.with(itemView)
                        .load(user.getAvatarURL())
                        .into(avatarImage);
            }
        }
    }

    // Checks the data in the list to see if it's the same and keep track of which object to change
    // and which are the same when scrolling
    private static final DiffUtil.ItemCallback<GenericUser> DIFF_CALLBACK =
        new DiffUtil.ItemCallback<GenericUser>() {

            // Id check
            @Override
            public boolean areItemsTheSame(@NonNull GenericUser oldData,
                                           @NonNull GenericUser newData) {
                return oldData.getId().equals(newData.getId());
            }

            // Data check
            @Override
            public boolean areContentsTheSame(@NonNull GenericUser oldData,
                                              @NonNull GenericUser newData) {
                return oldData.equals(newData);
            }
        };
}