package com.example.steamgiftmobile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.steamgiftmobile.EnteredGiveawayActivity;
import com.example.steamgiftmobile.MainActivity;
import com.example.steamgiftmobile.classes.Giveaway;
import com.example.steamgiftmobile.R;

import java.util.ArrayList;
import java.util.List;

// This class is used by recyclerView to adapt the items we want to show in the view
// In this case the giveaways
public class MainAdapter extends ListAdapter<Giveaway,MainAdapter.CustomViewHolder> {
    private Context context;
    private OnItemClickListener clickListener;
    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_LOAD_ITEM = 2;

    // Click listener to be able to click items in the list
    public interface OnItemClickListener {
        void onItemClicked(Giveaway giveaway);
    }

    public MainAdapter(Context context) {
        // Call to the diff checker (see end of file)
        super(DIFF_CALLBACK);
        this.context = context;
    }

    public MainAdapter(Context context,OnItemClickListener clickListener) {
        // Call to the diff checker (see end of file)
        super(DIFF_CALLBACK);
        this.context = context;
        this.clickListener = clickListener;
    }

    // returns item view type
    @Override
    public int getItemViewType(int position) {
        if(getCurrentList().get(position) != null){
            return VIEW_TYPE_ITEM;
        }else{
            return VIEW_LOAD_ITEM;
        }
    }

    // ViewHolder is used construct and recycle the various items we will pass and is
    // executed for each element visible on the screen (for example 8 object in the list ->
    // executed 8 times)
    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Go get the xml layout and inflate to create the view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        // We choose the right layout view depending on the viewType (data item or loading item)
        if(viewType == VIEW_TYPE_ITEM){
            View view = inflater.inflate(R.layout.item_main, parent, false);

            if(clickListener == null) return new MainViewHolder(view);

            return new MainViewHolder(view,clickListener);
        }else{
            View view = inflater.inflate(R.layout.load_item, parent, false);
            return new LoadViewHolder(view);
        }
    }

    //This method is used to render the object in the item list
    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {

        // We check the type of viewHolder to apply the right logic to it
        if(holder instanceof MainViewHolder){
            //Bind the ViewHolder to the item we want to render
            if(clickListener == null){
                // Clicklistener is null we are only showing the entered giveaway
                ((MainViewHolder) holder).bindTo(getItem(position));
            }
            else {
                //if the giveaway is expired
                //remove it from the list and update the list
                if(!getItem(position).isExpired()){
                    ((MainViewHolder) holder).bindTo(getItem(position));
                }else{
                    List<Giveaway> l = new ArrayList<Giveaway>(getCurrentList());
                    l.remove(position);
                    submitList(l);
                }
            }

            //if we reach the end of the list call the load data with the next webpage
            //only if it's not the last page
            if (position == getCurrentList().size() - 2 &&
                    getCurrentList().get(getCurrentList().size() - 1) == null){
                
                if(context instanceof MainActivity){
                    int pageCount = ((MainActivity) context).getPageCount();
                    ((MainActivity) context).loadData(++pageCount);
                }else if(context instanceof EnteredGiveawayActivity){
                    int pageCount = ((EnteredGiveawayActivity) context).getPageCount();
                    ((EnteredGiveawayActivity) context).loadData(++pageCount);
                }
            }
        }else{
            // At the end of the list we want a loading item type of view holder
            // Only the last item on the recycleView is instance of LoadViewHolder
            ((LoadViewHolder) holder).bindTo();
        }
    }

    // Custom class based on ViewHolder
    static class CustomViewHolder extends RecyclerView.ViewHolder{
        public CustomViewHolder(View itemView) {
            super(itemView);
        }
    }

    //MainViewHolder is the viewHolder we use to display data items
    static class MainViewHolder extends CustomViewHolder {
        private TextView textTitle;
        private TextView textCreator;
        private TextView textTime;
        private ImageView imageTime;
        private ImageView imageGame;
        private ImageView imageAvatar;
        private Giveaway giveaway;
        private LinearLayout creatorLine;

        MainViewHolder(@NonNull View itemView,OnItemClickListener clickListener) {
            super(itemView);

            // When a item is clicked we pass the object clicked
            itemView.setOnClickListener(v -> clickListener.onItemClicked(giveaway));

            textTitle = itemView.findViewById(R.id.textTitle);
            textCreator = itemView.findViewById(R.id.textCreator);
            textTime = itemView.findViewById(R.id.textTime);
            imageTime = itemView.findViewById(R.id.imageTime);
            imageTime.setImageResource(R.drawable.ic_access_time_24px);
            imageGame = itemView.findViewById(R.id.imageGame);
            imageGame.setImageResource(R.drawable.ic_no_image);
            imageAvatar = itemView.findViewById(R.id.imageAvatar);
            imageAvatar.setImageResource(R.drawable.ic_account_circle_24px);
        }

        MainViewHolder(@NonNull View itemView) {
            super(itemView);

            textTitle = itemView.findViewById(R.id.textTitle);
            textCreator = itemView.findViewById(R.id.textCreator);
            textTime = itemView.findViewById(R.id.textTime);
            imageTime = itemView.findViewById(R.id.imageTime);
            imageTime.setImageResource(R.drawable.ic_access_time_24px);
            imageGame = itemView.findViewById(R.id.imageGame);
            imageGame.setImageResource(R.drawable.ic_no_image);
            imageAvatar = itemView.findViewById(R.id.imageAvatar);
            imageAvatar.setImageResource(R.drawable.ic_account_circle_24px);
            creatorLine = itemView.findViewById(R.id.creator_line);
        }

        void bindTo(Giveaway giveaway) {

            this.giveaway = giveaway; // Needed by the clickListener

            textTitle.setText(giveaway.getTitle());
            textCreator.setText(giveaway.getCreator().getUsername());
            textTime.setText(giveaway.getRemainingTime());

            if(giveaway.getThumbnailImageURL() != null){
                Glide.with(itemView)
                        .load(giveaway.getThumbnailImageURL())
                        .into(imageGame);
            }

            if(!itemView.hasOnClickListeners()){
                creatorLine.setVisibility(View.INVISIBLE);

                if(giveaway.getTimestamp() == 0){
                    textTime.setText("Annullato");
                }
            }
        }
    }

    //LoadViewHolder is the viewHolder we use to display ProgressBar items
    // Only at the end of the list while more data is being loaded
    static class LoadViewHolder extends CustomViewHolder{
        private ProgressBar progressBar;

        LoadViewHolder(@NonNull View itemView){
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressbar);
        }

        void bindTo(){
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    // Checks the data in the list to see if it's the same and keep track of which object to change
    // and which are the same when scrolling
    private static final DiffUtil.ItemCallback<Giveaway> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Giveaway>() {

                // Id check
                @Override
                public boolean areItemsTheSame(@NonNull Giveaway oldData,
                                               @NonNull Giveaway newData) {
                    return oldData.getId().equals(newData.getId());
                }

                // Data check
                @Override
                public boolean areContentsTheSame(@NonNull Giveaway oldData,
                                                  @NonNull Giveaway newData) {
                    return oldData.equals(newData);
                }
            };
}
