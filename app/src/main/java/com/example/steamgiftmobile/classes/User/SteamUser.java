package com.example.steamgiftmobile.classes.User;

import com.example.steamgiftmobile.http.Parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Objects;

//Class which represents the user using the application
public class SteamUser extends User{
    // Methods are synchronized because we have a static global variable and we have to control
    // access

    private String sessionID;
    private int points;
    private String xsrfToken;

    public SteamUser(){
        super();
    }

    public SteamUser(String username,String avatarURL,int points,int level,String xsrfToken){
        super(username, level, avatarURL);
        setPoints(points);
        setXsrfToken(xsrfToken);
    }

    //clear the user instance (logout)
    public synchronized void clear(){
        this.username = null;
        this.sessionID = null;
        this.avatarURL = null;
        this.xsrfToken = null;
        this.points = -1;
        this.level = -1;
    }

    // Construct the object using the html
    public synchronized void extractFromHTML(String html){
        Document document = Jsoup.parseBodyFragment(html);
        Element body = document.body();

        Elements navbar = body.select(".nav__button-container");

        //Fetching the username
        Element userElement = navbar.last().select("a").first();
        String username = userElement.attr("href").substring(6);

        //Fetching avatar image
        String avatarImageURL = userElement.select("div")
                .first()
                .attr("style");

        avatarImageURL = Parser.parseHrefURI(avatarImageURL);

        //Fetching points
        Element userInfo = navbar.select("a[href=/account]").first();
        int points = Integer.parseInt(userInfo.select(".nav__points").text());

        float lvl = Float.parseFloat(userInfo.select("span")
                .last()
                .attr("title"));

        int level = (int) lvl;

        String token = Parser.extractXsrfToken(html);

        setUsername(username);
        setAvatarURL(avatarImageURL);
        setLevel(level);
        setPoints(points);
        setXsrfToken(token);
    }

    @Override
    protected synchronized void setUsername(String username) {
        if(username == null || username.isEmpty())
            throw new IllegalArgumentException("Empty string");

        this.username = username;
    }

    public String getXsrfToken() {
        return xsrfToken;
    }

    public synchronized void setXsrfToken(String xsrfToken) {
        this.xsrfToken = xsrfToken;
    }

    public String getSessionID() {
        return sessionID;
    }

    public synchronized void setSessionID(String id) {
        if(id.isEmpty())
            throw new IllegalArgumentException("Empty string. Null or not empty string required");
        sessionID = id;
    }

    @Override
    protected synchronized void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    public int getPoints() {
        return points;
    }

    public synchronized void setPoints(int points) {
        if(points < 0 ){
            this.points = 0;
            return;
        }

        this.points = points;
    }

    public int getLevel() {
        return level;
    }

    private synchronized void setLevel(int level) {
        if(level < 0){
            this.level = 0;
            return;
        }
        this.level = level;
    }

    @Override
    public String toString() {
        return "SteamUser{" +
                "username='" + username + '\'' +
                ", sessionID='" + sessionID + '\'' +
                ", avatarURL='" + avatarURL + '\'' +
                ", points=" + points +
                ", level=" + level +
                ", xsrfToken='" + xsrfToken + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SteamUser steamUser = (SteamUser) o;
        return getPoints() == steamUser.getPoints() &&
                getSessionID().equals(steamUser.getSessionID()) &&
                getXsrfToken().equals(steamUser.getXsrfToken());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSessionID(), getPoints(), getXsrfToken());
    }
}
