package com.example.steamgiftmobile.classes.User;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

// This class represents a generic SteamGift user (users that create giveaways)

public class GenericUser extends User implements Parcelable {
    private String id;

    public GenericUser(){ super(); }

    //Used when we show the various giveaways, we don't need other informations
    public GenericUser(String username){
        super(username);
    }

    public GenericUser(String username, int level, String avatarURL, String id){
        super(username,level,avatarURL);
        setId(id);
    }

    protected GenericUser(Parcel in) {
        username = in.readString();
    }

    public static final Creator<GenericUser> CREATOR = new Creator<GenericUser>() {
        @Override
        public GenericUser createFromParcel(Parcel in) {
            return new GenericUser(in);
        }

        @Override
        public GenericUser[] newArray(int size) {
            return new GenericUser[size];
        }
    };

    public String getId() {return id;}

    private void setId(String id) {
        if(id == null){
            throw new IllegalArgumentException("id cannot be null");
        }
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        GenericUser that = (GenericUser) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getId());
    }
}
