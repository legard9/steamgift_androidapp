package com.example.steamgiftmobile.classes;

import com.example.steamgiftmobile.classes.User.GenericUser;

import java.util.ArrayList;
import java.util.Objects;

// Extended giveaway class containing more informations regarding a giveaway
public class GiveawayExtended extends Giveaway{

    private String description;
    private ArrayList<Comment> comments;
    private int price;
    private int level;
    private boolean joined;

    // Basic constructor
    public GiveawayExtended(String id,
                            String title,
                            GenericUser creator,
                            long timestamp,
                            String thumbnailImageURL,
                            String description,
                            ArrayList<Comment> comments,
                            int price,
                            int level,
                            boolean joined){

        super(id,title,creator,timestamp,thumbnailImageURL);

        setComments(comments);
        setDescription(description);
        setPrice(price);
        setLevel(level);
        setJoined(joined);
    }

    // Constructor based on giveaway object
    public GiveawayExtended(Giveaway giveaway,
                            String description,
                            ArrayList<Comment> comments,
                            int price,
                            int level,
                            boolean joined){

        this(giveaway.getId(),
                giveaway.getTitle(),
                giveaway.getCreator(),
                giveaway.getTimestamp(),
                giveaway.getThumbnailImageURL(),
                description,
                comments,
                price,
                level,
                joined);
    }

    public boolean getJoined(){return joined;}

    public void setJoined(boolean joined){this.joined = joined;}

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        if(level>=0){
            this.level = level;
        }else{
            throw new IllegalArgumentException("Level must be non negative");
        }
    }

    public String getDescription() {
        return description;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    private void setComments(ArrayList<Comment> comments) {
        if(comments == null) throw new IllegalArgumentException("ArrayList must be not null");
        this.comments = comments;
    }

    public int getPrice() {
        return price;
    }

    private void setPrice(int price) {
        if(price < 0) throw new IllegalArgumentException("Price must be positive");
        this.price = price;
    }

    @Override
    public String toString() {
        return "GiveawayExtended{" +
                "description='" + description + '\'' +
                ", comments=" + comments +
                ", price=" + price +
                ", level=" + level +
                ", joined=" + joined +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GiveawayExtended)) return false;
        if (!super.equals(o)) return false;
        GiveawayExtended that = (GiveawayExtended) o;
        return getPrice() == that.getPrice() &&
                getLevel() == that.getLevel() &&
                getJoined() == that.getJoined() &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getComments(), that.getComments());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), description, comments, price, level, joined);
    }
}



