package com.example.steamgiftmobile.classes.User;

import java.util.Objects;

//abstract class that represents a user
public abstract class User {
    protected String username;
    protected int level;
    protected String avatarURL;

    public User(){}

    public User(String username){
       setUsername(username);
    }

    public User(String username, int level, String avatarURL){
        setUsername(username);
        setAvatarURL(avatarURL);
        setLevel(level);
    }

    public String getUsername(){
        return username;
    }

    protected void setUsername(String username) {
        if(username == null || username.isEmpty()){
            throw new IllegalArgumentException("username cannot be null or empty");
        }
        this.username = username;
    }

    public int getLevel() {
        return level;
    }

    private void setLevel(int level) {
        if(level<0){
            throw new IllegalArgumentException("level must be positive");
        }
        this.level = level;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    protected void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getLevel() == user.getLevel() &&
                getUsername().equals(user.getUsername()) &&
                Objects.equals(getAvatarURL(), user.getAvatarURL());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUsername(), getLevel(), getAvatarURL());
    }
}
