package com.example.steamgiftmobile.classes;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

//Class used to store session informations about the user to prevent login every time the user
//closes the app
public class Session {

    private SharedPreferences preferences;

    public Session(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setString(String key,String value) {
        preferences.edit().putString(key, value).apply();
    }

    public String getString(String key) {
        String value = preferences.getString(key,"");
        return value;
    }
}