package com.example.steamgiftmobile.classes;


import android.os.Parcel;
import android.os.Parcelable;

import com.example.steamgiftmobile.classes.User.GenericUser;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.TimeZone;

// Class representing a general giveaway
// Parcelable so we can pass it through the various activities when needed
public class Giveaway implements Parcelable{

    private String id;
    private String thumbnailImageURL;
    private String title;
    private GenericUser creator;
    private long timestamp;

    public Giveaway(String id, String title, GenericUser creator, long timestamp, String thumbnailImageURL){

        setCreator(creator);
        setId(id);
        setTitle(title);
        setTimestamp(timestamp);
        setThumbnailImageURL(thumbnailImageURL);
    }

    protected Giveaway(Parcel in) {
        id = in.readString();
        thumbnailImageURL = in.readString();
        title = in.readString();
        timestamp = in.readLong();
        creator = in.readParcelable(GenericUser.class.getClassLoader());
    }

    public static final Creator<Giveaway> CREATOR = new Creator<Giveaway>() {
        @Override
        public Giveaway createFromParcel(Parcel in) {
            return new Giveaway(in);
        }

        @Override
        public Giveaway[] newArray(int size) {
            return new Giveaway[size];
        }
    };

    public String getThumbnailImageURL(){ return this.thumbnailImageURL; }

    //null image is accepted
    public void setThumbnailImageURL(String imageURL) {
        this.thumbnailImageURL = imageURL;
    }

    public String getId(){
        return this.id;
    }

    private void setId(String id){
        if(id == null || id.isEmpty()){
            throw new IllegalArgumentException("Empty string");
        }
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    private void setTitle(String title){
        if (title == null || title.isEmpty()){
            throw new IllegalArgumentException("Empty string");
        }
        this.title = title;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    private void setTimestamp(long timestamp){
        if(timestamp < 0){
            throw new IllegalArgumentException("Negative number");
        }
        this.timestamp = timestamp;

    }

    public GenericUser getCreator() {
        return this.creator;
    }

    private void setCreator(GenericUser creator){
        if (creator == null){
            throw new IllegalArgumentException("creator field null");
        }
        this.creator = creator;
    }

    // get date and time of expiration
    public LocalDateTime getExpirationDate(){
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(this.timestamp), TimeZone.getDefault().toZoneId());
    }

    // get the remaining time in string format (eg. "23 minutes" or "3 hours")
    public String getRemainingTime(){

        LocalDateTime now = LocalDateTime.now();
        long diff = ChronoUnit.SECONDS.between(now,getExpirationDate());
        if(diff <0){
            return "Giveaway scaduto";
        }else if(diff < 60){
            return diff + " secondi";
        }
        else if(diff < 3600){
            return diff / 60 + " minuti";
        }
        else if (diff < 86400){
            return diff / 3600 + " ore";
        }
        else{
            return diff / 86400 + " giorni";
        }
    }

    // Checks if the giveaway is expired
    public boolean isExpired(){
        LocalDateTime now = LocalDateTime.now();

        long diff = ChronoUnit.SECONDS.between(now,getExpirationDate());

        return diff <= 0;
    }

    @Override
    public String toString() {
        return "Giveaway{" +
                "id='" + id + '\'' +
                ", imageURL='" + thumbnailImageURL + '\'' +
                ", title='" + title + '\'' +
                ", creator='" + creator.getUsername() + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Giveaway)) return false;
        Giveaway giveaway = (Giveaway) o;
        return timestamp == giveaway.timestamp &&
                id.equals(giveaway.id) &&
                Objects.equals(thumbnailImageURL, giveaway.thumbnailImageURL) &&
                title.equals(giveaway.title) &&
                creator.equals(giveaway.creator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getCreator(), getTimestamp());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(thumbnailImageURL);
        dest.writeString(title);
        dest.writeLong(timestamp);
        dest.writeParcelable(creator,flags);
    }
}
