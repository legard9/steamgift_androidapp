package com.example.steamgiftmobile.http.callbacks;

import com.example.steamgiftmobile.classes.User.GenericUserExtended;

//callback to handle the extraction of the userID from the user page
public interface UserProfileCallback {
    void onSuccess(GenericUserExtended user);
    void onError();
}
