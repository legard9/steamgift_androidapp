package com.example.steamgiftmobile.http.callbacks;


import com.example.steamgiftmobile.classes.Giveaway;

import java.util.ArrayList;
import java.util.List;

//callback that handles success and errors of HTTPClient
public interface GiveawayListCallback {
    void onSuccess(List<Giveaway> giveawayList,int endOfList);
    void onError();
}