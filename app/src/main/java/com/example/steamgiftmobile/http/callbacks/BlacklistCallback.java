package com.example.steamgiftmobile.http.callbacks;

import com.example.steamgiftmobile.classes.User.GenericUser;

import java.util.ArrayList;

//callback to handle the extraction of the blacklist
public interface BlacklistCallback {
    void onSuccess(ArrayList<GenericUser> users);
    void onEmpty();
    void onError();
}
