package com.example.steamgiftmobile.http;

import android.util.Log;

import com.example.steamgiftmobile.MainActivity;
import com.example.steamgiftmobile.classes.User.GenericUser;
import com.example.steamgiftmobile.classes.User.GenericUserExtended;
import com.example.steamgiftmobile.classes.Giveaway;
import com.example.steamgiftmobile.classes.GiveawayExtended;
import com.example.steamgiftmobile.http.callbacks.BlacklistCallback;
import com.example.steamgiftmobile.http.callbacks.GiveawayDetailsCallback;
import com.example.steamgiftmobile.http.callbacks.GiveawayListCallback;
import com.example.steamgiftmobile.http.callbacks.PostCallback;
import com.example.steamgiftmobile.http.callbacks.UserProfileCallback;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import okhttp3.Call;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

// Class that handles the HTTP requests, it's final so only one instance of it can be created
public final class HTTPClient {
    public static final HTTPClient INSTANCE = new HTTPClient();
    private static final String HOME_URL = "https://www.steamgifts.com/giveaways/search?page=";
    private static final String GIVEAWAY_DETAILS_URL = "https://www.steamgifts.com/giveaway/";
    private static final String USER_URL = "https://www.steamgifts.com/user/";
    private static final String POST_URL = "https://www.steamgifts.com/ajax.php";
    private static final String BLACKLIST_URL = "https://www.steamgifts.com/account/manage/blacklist";
    private static final String ENTERED_URL = "https://www.steamgifts.com/giveaways/entered/search?page=";
    private Executor executor;
    private OkHttpClient client;

    private HTTPClient() {
        executor = Executors.newFixedThreadPool(4);
        client = new OkHttpClient();
    }

    public void loadGiveaways(GiveawayListCallback callback){
        loadGiveaways(1,callback);
    }

    // builds the giveaway list after requesting the steamgift homepage
    public void loadGiveaways(int page, GiveawayListCallback callback) {
        Request request;

        //handling of HTTP GET request, is the user already logged?
        if(MainActivity.currentUser.getSessionID() != null){
            // if yes we pass the session ID to catch up informations about the user
            request = new Request.Builder()
                    .get()
                    .url(HOME_URL+page)
                    .header("cookie","PHPSESSID=" + MainActivity.currentUser.getSessionID())
                    .build();
        }else{// if not we just send a simple get
            request = new Request.Builder()
                    .get()
                    .url(HOME_URL+page)
                    .build();
        }

        executor.execute(() -> {
            try {
                //extracting the HTML body from the response
                Call call = client.newCall(request);
                Response response = call.execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {
                    //Parsing the HTML
                    String html = responseBody.string();
                    ArrayList<Giveaway> lista = Parser.giveawayListParser(html);

                    int endOfList = Parser.checkForEndOfList(html);

                    if (MainActivity.currentUser.getSessionID() != null){
                        MainActivity.currentUser.extractFromHTML(html);
                    }

                    //callback to handle the success
                    callback.onSuccess(lista,endOfList);
                    return;
                }
            }catch (IOException e) {
                Log.e("HTTPClient", "Error loading giveaways", e);
                e.printStackTrace();
            }
            callback.onError();
        });
    }

    // HTTP GET of a specific giveaway page (Giveaway object is needed to retrieve the id)
    public void loadGiveawayDetails(Giveaway giveaway, GiveawayDetailsCallback callback){
        //handling of HTTP GET request
        Request request;
        if(MainActivity.currentUser.getSessionID()!=null){//user logged in
            request = new Request.Builder()
                    .get()
                    .url(GIVEAWAY_DETAILS_URL+giveaway.getId()+"/")
                    .header("cookie","PHPSESSID=" + MainActivity.currentUser.getSessionID())
                    .build();
        }else{//user not logged in
            request = new Request.Builder()
                    .get()
                    .url(GIVEAWAY_DETAILS_URL+giveaway.getId()+"/")
                    .build();
        }

        executor.execute(() -> {
            try {
                //extracting the HTML body from the response
                Call call = client.newCall(request);
                Response response = call.execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {
                    //Parsing the HTML
                    String html = responseBody.string();
                    GiveawayExtended giveawayExtended = Parser.giveawayDetailsParser(html,giveaway);

                    //callback to handle the success
                    callback.onSuccess(giveawayExtended);

                    return;
                }
            }catch (IOException e) {
                Log.e("HTTPClient", "Error loading giveaway details", e);
                e.printStackTrace();
            }
            callback.onError(giveaway);
        });
    }

    public void loadEnteredGiveaways(int page, GiveawayListCallback callback){
        Request request;

        request = new Request.Builder()
                .get()
                .url(ENTERED_URL+page)
                .header("cookie","PHPSESSID=" + MainActivity.currentUser.getSessionID())
                .build();

        executor.execute(() -> {
            try {
                //extracting the HTML body from the response
                Call call = client.newCall(request);

                Response response = call.execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {
                    //Parsing the HTML
                    String html = responseBody.string();
                    ArrayList<Giveaway> lista = Parser.enteredGiveawayListParser(html);

                    int endOfList = Parser.checkForEndOfList(html);

                    //callback to handle the success
                    callback.onSuccess(lista,endOfList);
                    return;
                }
            }catch (IOException e) {
                Log.e("HTTPClient", "Error loading entered giveaways", e);
                e.printStackTrace();
            }
            callback.onError();
        });
    }

    // HTTP POST to enter or exit a giveaway, action specify which one we choose
    public void enterExitGiveaway(GiveawayExtended giveaway, String action, PostCallback callback){
        //build the request body giving the action to perform, the id for giveaway and xsrf_token that depends on current user and giveaway
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("do", action)
                .addFormDataPart("code",giveaway.getId())
                .addFormDataPart("xsrf_token", MainActivity.currentUser.getXsrfToken())
                .build();

        // build the request passing body and session id
        Request request;
        request = new Request.Builder()
                .post(requestBody)
                .url(POST_URL)
                .header("cookie","PHPSESSID=" + MainActivity.currentUser.getSessionID())
                .build();

        executor.execute(() -> {
            try {
                Call call = client.newCall(request);
                Response response = call.execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {
                    callback.onSuccess(responseBody.toString());
                    return;
                }
            }catch (IOException e) {
                if(action.equals("entry_insert")){
                    Log.e("HTTPClient", "Error entering giveaway", e);
                }else{
                    Log.e("HTTPClient", "Error exiting giveaway", e);
                }
                e.printStackTrace();
            }
            callback.onError();
        });
    }

    // Function to get a user profile (for now it only returns the user ID)
    public void loadGenericUserProfile(String username, UserProfileCallback callback){
        String creatorURL = USER_URL + username;

        //First we access the user page to retrieve the user ID
        Request request = new Request.Builder()
                .get()
                .url(creatorURL)
                .header("cookie","PHPSESSID=" + MainActivity.currentUser.getSessionID())
                .build();

        executor.execute(() -> {
            try {
                //extracting the HTML body from the response
                Call call = client.newCall(request);
                Response response = call.execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {
                    //Parsing the HTML
                    String html = responseBody.string();

                    //Actually extracting the user ID from the user page
                    GenericUserExtended user = Parser.parseUserProfile(html,username);

                    callback.onSuccess(user);
                    return;
                }
            }catch (IOException e) {
                Log.e("HTTPClient", "Error loading user page", e);
                e.printStackTrace();
            }
            callback.onError();
        });
    }

    // post request to add/remove a user to the blacklist
    public void postBlacklist(String userID, String action, PostCallback callback){
        // Building the request body
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("do", "blacklist")
                .addFormDataPart("child_user_id",userID)
                .addFormDataPart("xsrf_token", MainActivity.currentUser.getXsrfToken())
                .addFormDataPart("action", action)
                .build();

        Request request = new Request.Builder()
                .post(requestBody)
                .url(POST_URL)
                .header("cookie","PHPSESSID=" + MainActivity.currentUser.getSessionID())
                .build();

        executor.execute(() -> {
            try {
                Call call = client.newCall(request);
                Response response = call.execute();
                ResponseBody responseBody = response.body();

                if(responseBody!=null){
                    callback.onSuccess(responseBody.toString());
                }else{
                    callback.onError();
                }

                return;
            }catch (IOException e) {
                Log.e("HTTPClient", "Error during blacklist request", e);
                e.printStackTrace();
            }
            callback.onError();
        });
    }

    // Function to load the blacklist from the site
    public void loadBlacklist(BlacklistCallback callback){

        Request request = new Request.Builder()
                .get()
                .url(BLACKLIST_URL)
                .header("cookie","PHPSESSID=" + MainActivity.currentUser.getSessionID())
                .build();

        executor.execute(() -> {
            try {
                //extracting the HTML body from the response
                Call call = client.newCall(request);
                Response response = call.execute();
                ResponseBody responseBody = response.body();

                if (responseBody != null) {
                    //Parsing the HTML
                    String html = responseBody.string();

                    //Actually extracting the user ID from the user page
                    ArrayList<GenericUser> users = Parser.extractBlackList(html);

                    //check if the list is empty
                    if (users.get(0).equals(new GenericUser("dummy",0,"dummy","dummy"))){
                        callback.onEmpty();
                    }else{
                        if(users.size()>0){
                            callback.onSuccess(users);
                        }else{
                            callback.onError();
                        }
                    }
                    return;
                }
            }catch (IOException e) {
                Log.e("HTTPClient", "Error loading blacklist page", e);
                e.printStackTrace();
            }
            callback.onError();
        });
    }

}






















