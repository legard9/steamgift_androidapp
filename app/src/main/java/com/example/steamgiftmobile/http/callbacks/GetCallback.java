package com.example.steamgiftmobile.http.callbacks;

//Callback to handle get requests
public interface GetCallback {
    public void onSuccess();
    public void onError();
}
