package com.example.steamgiftmobile.http;

import com.example.steamgiftmobile.MainActivity;
import com.example.steamgiftmobile.classes.Comment;
import com.example.steamgiftmobile.classes.User.GenericUser;
import com.example.steamgiftmobile.classes.User.GenericUserExtended;
import com.example.steamgiftmobile.classes.Giveaway;
import com.example.steamgiftmobile.classes.GiveawayExtended;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

    // parsing the giveaway list from the HTML
    public static ArrayList<Giveaway> giveawayListParser (String html){
        ArrayList<Giveaway> giveaways = new ArrayList<Giveaway>();

        Document document = Jsoup.parseBodyFragment(html);
        document.select("div.pinned-giveaways__inner-wrap").remove();
        Element body = document.body();
        Elements giveawayBlocks = body.getElementsByClass("giveaway__row-outer-wrap");

        //parsing the data for each giveaway from HTML
        for (Element giveawayBlock : giveawayBlocks) {
            Element titleElement = giveawayBlock
                    .getElementsByClass("giveaway__heading__name")
                    .first();
            String title = titleElement.ownText();

            Element creatorElement = giveawayBlock
                    .getElementsByClass("giveaway__username")
                    .first();
            GenericUser creator = new GenericUser(creatorElement.ownText());

            Element remainingTimeElement = giveawayBlock
                    .getElementsByClass("giveaway__columns")
                    .first()
                    .child(0)
                    .child(1);

            long timestamp = Long.parseLong(remainingTimeElement.attr("data-timestamp"));

            String id = titleElement.attributes()
                    .get("href")
                    .split("/")
                    [2];

            Element gameImageElement = giveawayBlock
                    .getElementsByClass("giveaway_image_thumbnail")
                    .first();

            String strURL = null;

            if(gameImageElement != null){
                strURL = gameImageElement.attr("style");

                // Extract image URL
                strURL = parseHrefURI(strURL);
            }
            Giveaway giveaway = new Giveaway(id, title, creator, timestamp,strURL);
            giveaways.add(giveaway);
        }
        return giveaways;
    }

    // Parsing the giveaway details page from the HTML and extend the basic giveaway
    public static GiveawayExtended giveawayDetailsParser(String html,Giveaway giveaway){

        Document document = Jsoup.parseBodyFragment(html);
        Element body = document.body();

        String pricestr = body.getElementsByClass("featured__heading__small")
                .first()
                .ownText();

        // Sometimes the price is the second element
        if(pricestr.contains("Copies")){
            pricestr = body.getElementsByClass("featured__heading__small")
                    .get(1)
                    .ownText();
        }

        int price = Integer.parseInt(pricestr
                .replace("(","")
                .replace("P)",""));

        Element descriptionElement = body
                .getElementsByClass("markdown markdown--resize-body")
                .first();

        String description = null;

        if(descriptionElement != null){
            description = descriptionElement.text();
        }

        //parse the image in the details page and substitute to the previous one because this is
        //bigger and prettier
        Element gameImageElement = body
                .getElementsByClass("global__image-outer-wrap global__image-outer-wrap--game-large")
                .first();
        String gameImageURL = null;
        if (gameImageElement != null){
            gameImageURL = gameImageElement.child(0).attr("src");
        }
        giveaway.setThumbnailImageURL(gameImageURL);

        // extracting level required to enter the giveaway
        int level = 0;
        //cases for level greater or lower than the user level or level not required for the giveaway
        Elements levelElements = body.getElementsByClass(
                "featured__column featured__column--contributor-level featured__column--contributor-level--positive");
        if (levelElements.size() == 0) {
            levelElements = body.getElementsByClass(
                    "featured__column featured__column--contributor-level featured__column--contributor-level--negative");
        }
        if (levelElements.size() != 0){
            String levelString = levelElements.first().ownText();
            String[] levelParts = levelString.split(" ");
            levelString = levelParts[1].replace("+","");
            level = Integer.parseInt(levelString);
        }

        // checking if the user has entered the giveaway yet
        boolean joined = false;
        Element joinedElement = body.selectFirst(".sidebar form");
        if(joinedElement != null){
            if(joinedElement.select(".sidebar__entry-insert").hasClass("is-hidden")){
                joined = true;
            }
        }

        //TODO: Gestione commenti ancora da implementare
        ArrayList<Comment> comments = new ArrayList<Comment>();
        GiveawayExtended giveawayExtended = new GiveawayExtended(giveaway,
                description,
                comments,
                price,
                level,
                joined);

        return giveawayExtended;
    }

    public static ArrayList<Giveaway> enteredGiveawayListParser (String html){
        ArrayList<Giveaway> giveaways = new ArrayList<Giveaway>();

        Document document = Jsoup.parseBodyFragment(html);

        Element body = document.body();

        Element tableElement = body.getElementsByClass("table__rows").first();
        Elements giveawayElements = tableElement.getElementsByClass("table__row-inner-wrap");

        for (Element giveawayElement : giveawayElements) {
            //extract thumbnail image
            Element avatarElement = giveawayElement.getElementsByClass("table_image_thumbnail").first();
            String thumbnailImageURL = null;

            if ( avatarElement != null){
                thumbnailImageURL = parseHrefURI(avatarElement.attr("style"));
            }

            //extract title
            Element titleElement = giveawayElement.getElementsByClass("table__column__heading").first();
            String title = titleElement.ownText();

            String id = titleElement.attr("href").split("/")[2];

            long timestamp;

            try{
                Element timestampElement = giveawayElement
                        .getElementsByClass("table__column--width-fill")
                        .first()
                        .child(1)
                        .child(0);

                timestamp = Long.parseLong(timestampElement.attr("data-timestamp"));

            }catch(IndexOutOfBoundsException e){
                timestamp = 0;
            }

            GenericUser creator = new GenericUser();

            Giveaway giveaway = new Giveaway(id,title,creator,timestamp,thumbnailImageURL);
            giveaways.add(giveaway);
        }

        return giveaways;
    }

    public static int checkForEndOfList(String html){
        Document document = Jsoup.parseBodyFragment(html);

        Element body = document.body();

        Element pagesSection = body.getElementsByClass("pagination__navigation").first();
        String last = pagesSection.getElementsByTag("a").last().attr("data-page-number");

        if(last == null || last.equals("")){
            return 1;
        }
        return Integer.parseInt(last);
    }

    public static GenericUserExtended parseUserProfile(String html, String username){

        GenericUserExtended user;
        String userId;
        String avatar;

        Document document = Jsoup.parseBodyFragment(html);
        Element body  = document.body();

        //If the user is trying to view his own profile we can't get his id
        //This custom id value helps to understand when we are viewing the personal page
        if(username.equals(MainActivity.currentUser.getUsername())){
            userId = "currentUser";
            avatar = MainActivity.currentUser.getAvatarURL();
        }else{
            Element IDElement = body.getElementsByClass("sidebar__shortcut__blacklist")
                    .first();

            IDElement = IDElement.selectFirst("form");
            IDElement = IDElement.child(2);

            userId = IDElement.attr("value");

            String avatarTag = body.getElementsByClass("global__image-inner-wrap").first().attr("style");

            avatar = parseHrefURI(avatarTag);
        }

        Elements featureRows = body.getElementsByClass("featured__table__row__right");

        String role = featureRows.get(0).child(0).ownText();
        String lastOnline = featureRows.get(1).child(0).ownText();
        String registered = featureRows.get(2).child(0).ownText();
        String commentsstr = featureRows.get(3).ownText();
        String enteredstr = featureRows.get(4).ownText();
        String wonstr = featureRows.get(5).child(0).child(0).child(0).ownText();
        String sentstr = featureRows.get(6).child(0).child(0).child(0).ownText();
        String levelAttr = featureRows.get(7).child(0).attr("data-ui-tooltip");

        Pattern pattern = Pattern.compile(".*\"name\" : \"(.).*", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(levelAttr);

        String levelstr = null;

        if(matcher.find()) {
            levelstr = matcher.group(1);
        }

        int comments = Integer.parseInt(commentsstr.replace(",",""));
        int entered = Integer.parseInt(enteredstr.replace(",",""));
        int won = Integer.parseInt(wonstr.replace(",",""));
        int sent = Integer.parseInt(sentstr.replace(",",""));
        int level = Integer.parseInt(levelstr);

        user = new GenericUserExtended(username,
                level,
                avatar,
                userId,
                role,
                lastOnline,
                registered,
                comments,
                entered,
                sent,
                won);

        return user;
    }

    // Parsing of the href URI field using regex to get a HTTP link
    // return null if no match is found
    public static String parseHrefURI(String str){

        Pattern pattern = Pattern.compile("https.*jpg", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(str);

        if(matcher.find()) {
            str = matcher.group(0);
        } else {
            str = null;
        }

        return str;
    }

    //Function to extract the xsfr token (possible from multiple pages)
    public static String extractXsrfToken(String html){
        Document document = Jsoup.parseBodyFragment(html);
        Element body = document.body();

        Element xsfrElement = body.select("input[name=xsrf_token]").first();
        String xsfrToken = null;

        if (xsfrElement != null){
            xsfrToken = xsfrElement.attr("value");
        }

        return xsfrToken;
    }

    //Function to extract the blacklist from the site
    public static ArrayList<GenericUser> extractBlackList(String html){
        Document document = Jsoup.parseBodyFragment(html);
        Element body = document.body();

        ArrayList<GenericUser> users = new ArrayList<GenericUser>();

        Element tableElement = body.getElementsByClass("table__rows").first();
        Elements usersElements = tableElement.getElementsByClass("table__row-inner-wrap");

        for (Element userElement : usersElements) {
            //extract avatar
            Element avatarElement = userElement.getElementsByClass("table_image_avatar").first();
            String avatarImageURL = null;

            if ( avatarElement != null){
                avatarImageURL = parseHrefURI(avatarElement.attr("style"));
            }

            //extract username
            Element usernameElement = userElement.getElementsByClass("table__column__heading").first();
            String username = usernameElement.ownText();

            //extract ID
            Element IDElement = userElement.selectFirst("form").child(6);
            String userId = null;

            if (IDElement != null){
                userId = IDElement.attr("value");
            }

            //from here we can't extract the user level, but for blacklist purpose it's not needed,
            //so it's set at 0
            GenericUser user = new GenericUser(username,0,avatarImageURL,userId);
            users.add(user);
        }

        //add a control dummy user to check if empty
        if(users.size()==0){
            users.add(new GenericUser("dummy",0,"dummy","dummy"));
        }
        return users;
    }
}
