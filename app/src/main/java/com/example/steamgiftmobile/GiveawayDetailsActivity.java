package com.example.steamgiftmobile;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.example.steamgiftmobile.classes.User.GenericUserExtended;
import com.example.steamgiftmobile.classes.Giveaway;
import com.example.steamgiftmobile.classes.GiveawayExtended;
import com.example.steamgiftmobile.http.callbacks.GiveawayDetailsCallback;
import com.example.steamgiftmobile.http.HTTPClient;
import com.example.steamgiftmobile.http.callbacks.PostCallback;
import com.example.steamgiftmobile.http.callbacks.UserProfileCallback;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

// In this activity is possible to consult the details of the selected giveaway
public class GiveawayDetailsActivity extends AppCompatActivity {

    private TextView textTitle;
    private TextView textCreator;
    private TextView textExpirationDate;
    private TextView textDescription;
    private ImageView avatarImage;
    private ImageView gameImage;
    private ProgressBar progressBar;
    private CardView card;
    private ExtendedFloatingActionButton efab;
    private TextView textLevel;
    private ProgressBar progressBarEFAB;
    private View separator1;
    private View separator2;
    private Button blacklistButton;
    private ProgressBar progressBarBlacklist;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_giveawaydetails);

        textTitle = findViewById(R.id.textTitle);
        textCreator = findViewById(R.id.textCreator);
        textExpirationDate = findViewById(R.id.textExpirationDate);
        textDescription = findViewById(R.id.textDescription);
        avatarImage = findViewById(R.id.imageAvatar);
        gameImage = findViewById(R.id.imageGame);
        progressBar = findViewById(R.id.progressBar);
        card = findViewById(R.id.card_view);
        efab = findViewById(R.id.extended_fab);
        textLevel = findViewById(R.id.textLevel);
        progressBarEFAB = findViewById(R.id.progressBarEFAB);
        separator1 = findViewById(R.id.separator1);
        separator2 = findViewById(R.id.separator2);
        blacklistButton = findViewById(R.id.blacklist_button);
        progressBarBlacklist = findViewById(R.id.progressBarBlacklist);

        extendGiveaway(getIntent()
                .getExtras()
                .getParcelable("com.example.steamgiftmobile.classes.Giveaway"));
    }

    // HTTP get of the giveaway page (giveaway object needed to costrunct the giveawayExtended object)
    private void extendGiveaway(Giveaway giveaway){
        HTTPClient.INSTANCE.loadGiveawayDetails(giveaway, new GiveawayDetailsCallback() {
            @Override
            public void onSuccess(GiveawayExtended giveawayExtended) {
                runOnUiThread(() -> {
                    // Now show the data in the view
                    showData(giveawayExtended);
                });
            }
            @Override
            public void onError(Giveaway giveaway) {
                runOnUiThread(() -> showErrorSnackbar(giveaway));
            }
        });
    }

    // Show the giveaway data
    private void showData(GiveawayExtended giveaway){
        textTitle.setText(giveaway.getTitle());
        textExpirationDate.setText(giveaway.getRemainingTime());
        textCreator.setText(giveaway.getCreator().getUsername());
        gameImage.setImageResource(R.drawable.ic_no_image);
        avatarImage.setImageResource(R.drawable.ic_account_circle_24px);
        textLevel.setText("Level " + giveaway.getLevel() + "+");

        if(giveaway.getThumbnailImageURL() != null){
            Glide.with(this)
                    .load(giveaway.getThumbnailImageURL())
                    .into(gameImage);
        }

        if(giveaway.getDescription() != null){
            textDescription.setText(giveaway.getDescription());
            textDescription.setVisibility(View.VISIBLE);
            separator1.setVisibility(View.VISIBLE);
        }

        //Showing widgets for logged in users
        if(MainActivity.currentUser.getSessionID() != null){
            showLoggedUserWidgets(giveaway);
        }

        card.setVisibility(View.VISIBLE);
        gameImage.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
    }

    //if the user is logged shows some more widgets
    private void showLoggedUserWidgets(GiveawayExtended giveaway){

        String joinText = "Partecipa (" + giveaway.getPrice() +"p)";
        String exitText = "Rimuovi (" + giveaway.getPrice() +"p)";

        //Set join button text based on giveaway status
        if(giveaway.getJoined()){
            efab.setText(exitText);
            efab.setBackgroundColor(Color.RED);
        }else{
            efab.setText(joinText);
            efab.setBackgroundColor(Color.GREEN);
        }

        efab.setVisibility(View.VISIBLE);
        separator2.setVisibility(View.VISIBLE);
        blacklistButton.setVisibility(View.VISIBLE);

        // Setting error message to show in case user tries to enter a giveaway without requirements
        String errorMessage = "";
        if(giveaway.getRemainingTime().equals("Giveaway scaduto")) {
            errorMessage = "Impossibile accedere: giveaway scaduto";
        }else if (MainActivity.currentUser.getPoints() < giveaway.getPrice()){
            errorMessage = "Punti utente insufficienti";
        }else if (MainActivity.currentUser.getLevel() < giveaway.getLevel()){
            errorMessage = "Livello utente insufficiente";
        }

        //points and level are enough to access the giveaway
        if(errorMessage.equals("")){
            efab.setOnClickListener(view -> {
                //the user is already in the giveaway so clicking exits the giveaway
                if(giveaway.getJoined()){
                    efab.setVisibility(View.INVISIBLE);
                    progressBarEFAB.setVisibility(View.VISIBLE);

                    HTTPClient.INSTANCE.enterExitGiveaway(giveaway, "entry_delete", new PostCallback() {

                        @Override
                        public void onSuccess(String response) {
                            runOnUiThread(() -> {
                                efab.setText(joinText);
                                efab.setBackgroundColor(Color.GREEN);
                                efab.setVisibility(View.VISIBLE);
                                progressBarEFAB.setVisibility(View.INVISIBLE);
                                giveaway.setJoined(false);

                                MainActivity.currentUser
                                        .setPoints(MainActivity.currentUser.getPoints()
                                                +giveaway.getPrice());
                            });
                        }

                        @Override
                        public void onError() {
                            runOnUiThread(() -> {
                                Snackbar snackBar = Snackbar.make(view,
                                        "Errore durante l'uscita dal giveaway",
                                        Snackbar.LENGTH_SHORT)
                                        .setAction("OK", view13 -> {
                                        });
                                snackBar.show();
                            });
                        }
                    });

                }else{
                    //the user is not in the giveaway yet so clicking enters the giveaway
                    efab.setVisibility(View.INVISIBLE);
                    progressBarEFAB.setVisibility(View.VISIBLE);

                    HTTPClient.INSTANCE.enterExitGiveaway(giveaway, "entry_insert", new PostCallback() {

                        @Override
                        public void onSuccess(String response) {
                            runOnUiThread(() -> {
                                efab.setText(exitText);
                                efab.setBackgroundColor(Color.RED);
                                efab.setVisibility(View.VISIBLE);
                                progressBarEFAB.setVisibility(View.INVISIBLE);
                                giveaway.setJoined(true);
                                MainActivity.currentUser
                                        .setPoints(MainActivity.currentUser.getPoints()
                                                -giveaway.getPrice());
                            });
                        }

                        @Override
                        public void onError() {
                            runOnUiThread(() -> {
                                Snackbar snackBar = Snackbar.make(view,
                                        "Errore durante l'ingresso nel giveaway",
                                        Snackbar.LENGTH_SHORT)
                                        .setAction("OK", view14 -> {
                                        });
                                snackBar.show();
                            });
                        }
                    });
                }
            });
        }else{
            //lacking of points or levels to enter the giveaway
            efab.setBackgroundColor(Color.rgb(140,140,140));
            String finalErrorMessage = errorMessage;

            efab.setOnClickListener(v -> {
                Snackbar snackBar = Snackbar.make(v,
                        finalErrorMessage,
                        Snackbar.LENGTH_SHORT)
                        .setAction("OK", view -> {
                        });
                snackBar.show();
            });
        }

        //setting up blacklist button listeners
        blacklistButton.setOnClickListener(view -> {
            //showing progressbar when clicks and temporarily hide the button
            progressBarBlacklist.setVisibility(View.VISIBLE);
            blacklistButton.setVisibility(View.GONE);

            HTTPClient.INSTANCE.loadGenericUserProfile(giveaway.getCreator().getUsername(), new UserProfileCallback() {
                @Override
                public void onSuccess(GenericUserExtended user) {
                    runOnUiThread(() -> {
                        //User is already blocked?
                        if(blacklistButton.getText().equals("Blocca utente")){
                            // No, blocking the user

                            HTTPClient.INSTANCE.postBlacklist(user.getId(), "insert", new PostCallback() {
                                @Override
                                public void onSuccess(String response) {
                                    runOnUiThread(() -> {
                                        blacklistButton.setText("Sblocca utente");
                                        blacklistButton.setVisibility(View.VISIBLE);
                                        progressBarBlacklist.setVisibility(View.GONE);
                                    });
                                }

                                @Override
                                public void onError() {
                                    runOnUiThread(() -> {
                                        blacklistButton.setVisibility(View.VISIBLE);
                                        progressBarBlacklist.setVisibility(View.GONE);

                                        Snackbar snackBar = Snackbar.make(view,
                                                "Errore durante l'aggiunta alla blacklist",
                                                Snackbar.LENGTH_SHORT)
                                                .setAction("OK", view12 -> { });
                                        snackBar.show();
                                    });
                                }
                            });
                        }else{
                            // Yes, unblocking the user
                            HTTPClient.INSTANCE.postBlacklist(user.getId(), "delete", new PostCallback() {
                                @Override
                                public void onSuccess(String response) {
                                    runOnUiThread(() -> {
                                        blacklistButton.setText("Blocca utente");
                                        blacklistButton.setVisibility(View.VISIBLE);
                                        progressBarBlacklist.setVisibility(View.GONE);
                                    });
                                }

                                @Override
                                public void onError() {
                                    runOnUiThread(() -> {
                                        blacklistButton.setVisibility(View.VISIBLE);
                                        progressBarBlacklist.setVisibility(View.GONE);

                                        Snackbar snackBar = Snackbar.make(view,
                                                "Errore durante la rimozione dalla blacklist",
                                                Snackbar.LENGTH_SHORT)
                                                .setAction("OK", view1 -> { });
                                        snackBar.show();
                                    });
                                }
                            });
                        }
                    });
                }
                @Override
                public void onError() {
                    runOnUiThread(() -> {
                        blacklistButton.setVisibility(View.VISIBLE);
                        progressBarBlacklist.setVisibility(View.GONE);

                        Snackbar snackBar = Snackbar.make(view,
                                "Errore durante l'aggiunta alla blacklist",
                                Snackbar.LENGTH_SHORT)
                                .setAction("OK", view15 -> { });
                        snackBar.show();
                    });
                }
            });
        });
    }

    // Start the activity passing the intent containing the object we want to show
    public static void startGiveawayDetailsActivity(Activity activity, Giveaway giveaway){
        Intent intent = new Intent(activity,GiveawayDetailsActivity.class);
        intent.putExtra("com.example.steamgiftmobile.classes.Giveaway", giveaway);
        activity.startActivity(intent);
    }

    // Show error snackbar to let the user retry in case of internet issues
    private void showErrorSnackbar(Giveaway giveaway) {
        View rootView = findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(rootView,
                "Errore durante il caricamento della pagina", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Riprova", v -> {
            extendGiveaway(giveaway);
            snackbar.dismiss();
        });
        snackbar.show();
    }
}
































