package com.example.steamgiftmobile;

import com.example.steamgiftmobile.classes.User.GenericUser;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class GenericUserTest{
    final String username = "username";
    final String avatar = "avatar";
    final String id = "1";
    final int level = 3;
    private GenericUser user;

    @Before
    public void construct(){
        user = new GenericUser(username,level,avatar,id);
    }

    @Test
    public void getTests(){
        assertEquals(user.getUsername(), "username");
        assertEquals(user.getAvatarURL(), "avatar");
        assertEquals(user.getId(), "1");
        assertEquals(user.getLevel(),3);
    }

    @Test
    public void equalsTest(){
        GenericUser user2 = user;
        assertTrue(user.equals(user2));

        GenericUser user3 = new GenericUser(username,level, avatar, id);
        assertTrue(user.equals(user3));

        GenericUser user4 = new GenericUser(username,level, avatar, "2");
        assertFalse(user.equals(user4));
    }

    @Test
    public void ExceptionTest(){
        assertThrows(IllegalArgumentException.class, () -> {
            GenericUser u = new GenericUser(null,-1,null,null);
        });
    }

    @Test
    public void hashTest(){
        GenericUser user2 = user;
        assertEquals(user.hashCode(), user2.hashCode());

        GenericUser user3 = new GenericUser(username,level, avatar, id);
        assertEquals(user.hashCode(), user3.hashCode());

        GenericUser user4 = new GenericUser(username,level, avatar, "2");
        assertThat(user.hashCode(), is(not(user4.hashCode())));
    }
}
