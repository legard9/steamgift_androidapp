package com.example.steamgiftmobile;

import com.example.steamgiftmobile.classes.User.SteamUser;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class SteamUserTest {
    
    final int level = 5;
    final int points = 500;
    final String avatarImageURL = "URL";
    final String username = "user";
    final String token = "token";
    private SteamUser user;

    @Before
    public void construct(){
        user = new SteamUser(username, avatarImageURL, points, level,token);
        user.setSessionID("1");
    }

    @Test
    public void extractionTest(){
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("logged_homepage.html");
        StringBuilder textBuilder = new StringBuilder();

        try (Reader reader = new BufferedReader(new InputStreamReader
                (in, Charset.forName(StandardCharsets.UTF_8.name())))) {
            int c = 0;
            while ((c = reader.read()) != -1) {
                textBuilder.append((char) c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        String html = textBuilder.toString();

        SteamUser userExtracted = new SteamUser();
        userExtracted.extractFromHTML(html);

        String username = "legard9";
        String avatarImageURL = "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/f3/f3a6c8df6876799171f9cb40d30a8e3200371a2a_medium.jpg";
        int points = 400;
        int level = 0;
        String xsrfToken = "222734aa097cd82693206fb13bd42067";
        String sessionID = "1";

        SteamUser user = new SteamUser(username, avatarImageURL, points, level,xsrfToken);

        user.setSessionID(sessionID);
        userExtracted.setSessionID(sessionID);

        assertEquals(userExtracted,user);
    }

    @Test
    public void getTests(){
        assertEquals(user.getUsername(), "user");
        assertEquals(user.getAvatarURL(), "URL");
        assertEquals(user.getLevel(), 5);
        assertEquals(user.getPoints(), 500);
        assertEquals(user.getSessionID(), "1");
        assertEquals(user.getXsrfToken(), "token");
    }

    @Test
    public void throwsTest(){
        assertThrows(IllegalArgumentException.class, ()->{
            user.setSessionID("");});
    }

    @Test
    public void equalsTest(){
        SteamUser user2 = user;
        assertTrue(user.equals(user2));
        SteamUser user3 = new SteamUser(username, avatarImageURL, points, level,token);
        user3.setSessionID(user.getSessionID());
        assertTrue(user.equals(user3));
        SteamUser user4 = new SteamUser(username, avatarImageURL, points, level+1,token);
        assertFalse(user.equals(user4));
    }

    @Test
    public void hashTest(){
        SteamUser user2 = user;
        assertEquals(user.hashCode(), user2.hashCode());
        SteamUser user3 = new SteamUser(username, avatarImageURL, points, level,token);
        user3.setSessionID(user.getSessionID());
        assertEquals(user.hashCode(), user3.hashCode());
        SteamUser user4 = new SteamUser(username, avatarImageURL, points, level+1,token);
        assertThat(user.hashCode(), is(not(user4.hashCode())));
    }

    @Test
    public void logoutTest(){
        user.clear();

        assertEquals(user.getUsername(), null);
        assertEquals(user.getPoints(), -1);
        assertEquals(user.getLevel(), -1);
        assertEquals(user.getAvatarURL(), null);
        assertEquals(user.getSessionID(),null);
    }
}
