package com.example.steamgiftmobile;

import com.example.steamgiftmobile.classes.Comment;
import com.example.steamgiftmobile.classes.User.GenericUser;
import com.example.steamgiftmobile.classes.Giveaway;
import com.example.steamgiftmobile.classes.GiveawayExtended;
import com.example.steamgiftmobile.http.Parser;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ParserTest {

    public String html;

    private final String id1 = "oRXJ2";
    private final String title1 = "Chris Sawyer's Locomotion™";
    private final GenericUser creator1 = new GenericUser("Badger8154");
    private final long timestamp1 = 1616520600;
    private final String url1 = "https://steamcdn-a.akamaihd.net/steam/apps/356430/capsule_184x69.jpg";

    private final String id2 = "8LCF6";
    private final String title2 = "The Walking Dead: 400 Days";
    private final GenericUser creator2 = new GenericUser("Badger8154");
    private final long timestamp2 = 1616520600;
    private final String url2 = "https://steamcdn-a.akamaihd.net/steam/apps/207620/capsule_184x69.jpg";

    private final String id3 = "YTAx6";
    private final String title3 = "Monster Truck Championship\n";
    private final GenericUser creator3 = new GenericUser("radixlectiuk");
    private final long timestamp3 = 1616939880;
    private final String url3 = "https://steamcdn-a.akamaihd.net/steam/apps/1209360/header_292x136.jpg";
    private final String description = null;
    private final ArrayList<Comment> comments = new ArrayList<Comment>();
    private final int price = 40;
    private final String avatarImageURL = "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/0c/0cc95826e6a7537d7c41cfefd9284e4998f3d2e5_medium.jpg";

    private int level = 0;
    private boolean joined = false;
    private String xsrfToken = "eiuowbewpo";


    private ArrayList<Giveaway> giveaways;

    @Before
    public void createParser(){

    }
    @Test
    public void parserHomepageTest(){
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("homepage.HTML");
        StringBuilder textBuilder = new StringBuilder();
        try (Reader reader = new BufferedReader(new InputStreamReader
                (in, Charset.forName(StandardCharsets.UTF_8.name())))) {
            int c = 0;
            while ((c = reader.read()) != -1) {
                textBuilder.append((char) c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String html = textBuilder.toString();
        ArrayList<Giveaway> lista = Parser.giveawayListParser(html);

        Giveaway giveaway1 = new Giveaway(id1,title1,creator1,timestamp1,url1);
        Giveaway giveaway2 = new Giveaway(id2,title2,creator2,timestamp2,url2);

        Giveaway el1 = lista.get(2);
        Giveaway el2 = lista.get(3);

        assertTrue(el1.equals(giveaway1) && el2.equals(giveaway2));
    }

    @Test
    public void giveawayDetailsParserTest(){
        Giveaway baseGiveaway = new Giveaway(id3,title3, creator3, timestamp3, url3);
        GiveawayExtended extended = new GiveawayExtended(baseGiveaway, description,
                comments, price, level,joined);

        InputStream in = this.getClass().getClassLoader().getResourceAsStream("details.html");
        StringBuilder textBuilder = new StringBuilder();
        try (Reader reader = new BufferedReader(new InputStreamReader
                (in, Charset.forName(StandardCharsets.UTF_8.name())))) {
            int c = 0;
            while ((c = reader.read()) != -1) {
                textBuilder.append((char) c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String html = textBuilder.toString();
        GiveawayExtended giveawayExtended = Parser.giveawayDetailsParser(html, baseGiveaway);

        assertEquals(giveawayExtended, extended);

    }
}



























