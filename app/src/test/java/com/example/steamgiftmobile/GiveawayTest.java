package com.example.steamgiftmobile;

import com.example.steamgiftmobile.classes.User.GenericUser;
import com.example.steamgiftmobile.classes.Giveaway;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

public class GiveawayTest {
    private Giveaway giveaway;
    private final String id = "id";
    private final String title = "titolo";
    private final GenericUser creator = new GenericUser("creatore");
    private final long timestamp = 300;
    private final String url = "URL";

    @Before
    public void createGiveaway(){
        giveaway = new Giveaway(id,title,creator,timestamp,url);
    }

    @Test
    public void getTests() {
        assertEquals(giveaway.getId(), id);
        assertEquals(giveaway.getTitle(), title);
        assertEquals(giveaway.getCreator(), creator);
        assertEquals(giveaway.getTimestamp(), timestamp);
        assertEquals(giveaway.getThumbnailImageURL(), url);
    }
    @Test
    public void throwsTest(){
        assertThrows(IllegalArgumentException.class, ()->{
            giveaway = new Giveaway("",title,creator,timestamp,url);});
        assertThrows(IllegalArgumentException.class, ()->{
            giveaway = new Giveaway(id,"",creator,timestamp,url);});
        assertThrows(IllegalArgumentException.class, ()->{
            giveaway = new Giveaway(id,title,null,timestamp,url);});
        assertThrows(IllegalArgumentException.class, ()->{
            giveaway = new Giveaway(id,title,creator,-3,url);});
        assertThrows(IllegalArgumentException.class, ()->{
            giveaway = new Giveaway(null,title,creator,timestamp,url);});
        assertThrows(IllegalArgumentException.class, ()->{
            giveaway = new Giveaway(id,null,creator,timestamp,url);});
        assertThrows(IllegalArgumentException.class, ()->{
            giveaway = new Giveaway(id,title,null,timestamp,url);});
    }
    @Test
    public void isExpiredTest(){
        assertTrue(giveaway.isExpired());
        long nowTimestamp = System.currentTimeMillis()/1000;
        giveaway = new Giveaway(id,title,creator,nowTimestamp + 120,url);
        assertFalse(giveaway.isExpired());
    }
    @Test
    public void equalsTest(){
        Giveaway giveaway2 = giveaway;
        assertTrue(giveaway.equals(giveaway2));
        Giveaway giveaway3 = new Giveaway(id,title,creator,timestamp,url);
        assertTrue(giveaway.equals(giveaway3));
        Giveaway giveaway4 = new Giveaway(id,title,creator,timestamp+1,url);
        assertFalse(giveaway.equals(giveaway4));
    }
    @Test
    public void hashTest(){
        Giveaway giveaway2 = giveaway;
        assertEquals(giveaway.hashCode(), giveaway2.hashCode());
        Giveaway giveaway3 = new Giveaway(id,title,creator,timestamp,url);
        assertEquals(giveaway.hashCode(), giveaway3.hashCode());
        Giveaway giveaway4 = new Giveaway(id,title,creator,timestamp+1,url);
        assertThat(giveaway.hashCode(), is(not(giveaway4.hashCode())));
    }
}
