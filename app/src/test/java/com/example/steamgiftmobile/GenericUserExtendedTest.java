package com.example.steamgiftmobile;

import com.example.steamgiftmobile.classes.User.GenericUser;
import com.example.steamgiftmobile.classes.User.GenericUserExtended;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThrows;

public class GenericUserExtendedTest {

    private GenericUserExtended userExtended;
    final String username = "username";
    final String avatar = "avatar";
    final String id = "1";
    final int level = 3;
    final String role = "member";
    final String online ="online";
    final String registered = "3 days ago";
    final int comments = 5;
    final int entered = 3000;
    final int sent = 4;
    final int won = 2;

    @Before
    public void construct(){
        userExtended = new GenericUserExtended(username,
                level,
                avatar,
                id,
                role,
                online,
                registered,
                comments,
                entered,
                sent,
                won);
    }

    @Test
    public void getTest(){
        assertEquals(userExtended.getRole(),role);
        assertEquals(userExtended.getLastOnline(),online);
        assertEquals(userExtended.getRegisteredSince(),registered);
        assertEquals(userExtended.getnComments(),comments);
        assertEquals(userExtended.getnEntered(),entered);
        assertEquals(userExtended.getnGiftsWon(),won);
        assertEquals(userExtended.getnGiftsSent(),sent);
    }

    @Test
    public void constructorTest(){
        GenericUser user = new GenericUser(userExtended.getUsername(),
                userExtended.getLevel(),
                userExtended.getAvatarURL(),
                userExtended.getId());

        GenericUserExtended derived = new GenericUserExtended(user,
                role,
                online,
                registered,
                comments,
                entered,
                sent,
                won);

        assertEquals(user.getId(),derived.getId());
        assertEquals(user.getUsername(),derived.getUsername());
        assertEquals(user.getAvatarURL(),derived.getAvatarURL());
        assertEquals(user.getLevel(),derived.getLevel());
    }

    @Test
    public void equalsTest(){
        GenericUserExtended u1 = new GenericUserExtended(username,
                level,
                avatar,
                id,
                role,
                online,
                registered,
                comments,
                entered,
                sent,
                won);

        assertEquals(userExtended,u1);

        GenericUserExtended u2 = new GenericUserExtended(username,
                level,
                avatar,
                id,
                role,
                online,
                registered,
                comments,
                65,
                sent,
                won);

        assertNotEquals(userExtended,u2);

        GenericUserExtended u3 = new GenericUserExtended(username,
                level,
                avatar,
                "89",
                role,
                online,
                registered,
                comments,
                entered,
                sent,
                won);

        assertNotEquals(userExtended,u3);
    }
}
