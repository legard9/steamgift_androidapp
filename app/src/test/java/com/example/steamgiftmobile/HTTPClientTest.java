package com.example.steamgiftmobile;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import com.example.steamgiftmobile.classes.Giveaway;
import com.example.steamgiftmobile.http.callbacks.GiveawayListCallback;
import com.example.steamgiftmobile.http.HTTPClient;

import org.junit.Test;

import java.util.List;

public class HTTPClientTest {

    @Test
    public void validSiteTest() {
        int page = 2;
        HTTPClient.INSTANCE.loadGiveaways(page, new GiveawayListCallback() {
            @Override
            public void onSuccess(List<Giveaway> giveawayList,int lastPage) {
                assertNotNull(giveawayList);
            }
            @Override
            public void onError() {}
        });
    }

}
