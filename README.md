# SteamGift_AndroidApp

Project regarding the creation of a mobile Android app for the SteamGift website.

**Architecture diagram**

![alt text](img/app_architecture.png "App architecture")

# Files

All files are commented to help the understanding of the code, here there's a brief summary of each file to help navigate them.

&nbsp;
## Adapters

**MainAdapter.java**

This file contains all the adapters that are used by the recyclerView object to adapt the items we want to show in the view (giveaways and loading indicators), bind them correctly and handle the current list when it ends so it can load new giveaways and display a loading bar in the meanwhile.

**BlackListAdapter.java**

This file contains the adapter used to handle the items in the recyclerview used in the blacklist activity (SteamGiftUser items).

**DividerItemDecoration.java**

This file represents a divider used in the RecyclerView to separate items.

&nbsp;
## Classes

**Session.java**

This class represents the Session object used to store informations regarding the logged user.

**Giveaway.java**

This class represents the Giveaway object and contains a GenericUser attribute, that is the creator of the Giveaway.

**GiveawayExtended.java**

This class extends Giveaway and represent the giveaway with additional details taken from the dedicated giveaway page.

**Comment.java**

This class will be used in the future and it's not implemented yet.

## Classes/User

**User.java**

This class is abstract and represents the user concept, it contains only the base informations.

**GenericUser**

This file represents a generic Steamgift.com user (eg. the creator of a giveaway).

**GenericUserExtended**

This file represents the extended version of a generic user, with all the informations we can extract from the profile page.

**SteamUser.java**

This file represents the application user object which contains the user informations, and the methods to contruct the object, obtained after logging in from Steam.

&nbsp;
## HTTP

**HTTPClient.java**

This file handles the HTTP requests and responses to the SteamGifts webpage.

**Parser.java**

This file parse the html of the SteamGift webpage to extract the needed informations and construct the correspondent objects.

## HTTP/callbacks

**GetCallback.java**

This file is an interface for the callback that is implemented to represent success or error of a generic Post request.

**GiveawayListCallback.java**

This file is an interface for the callback that is implemented to represent success or error of the giveaways of a new webpage getting loaded.

**GiveawayDetailsCallback.java**

This file is an interface for the callback that is implemented to represent success or error of the loading of the giveaway details dedicated webpage.

**PostCallback.java**

This file is an interface for the callback that is implemented to represent success or error of a post request to the Steamgift.com website.

**UserProfileCallback.java**

This file is an interface for the callback that is implemented to represent success or error of the loading of the user page of a generic user.

**BlacklistCallback.java**

This file is an interface for the callback that is implemented to represent success or error of the loading of the blacklist of the current user.

&nbsp;
## Activities (no folder)

**MainActivity.java**

This file contains the logic of the main activity of the app, as well as the burger menu.

**GiveawayDetailsActivity.java**

This file contains the logic of the activity containing the giveaway details pages.

**LoginActivity.java**

This file contains the logic behind the login page (which is a webview). This activity just renders a web page (the Steam login page), and on success, or failure, returns to the main activity with the user object obtained.

**BlacklistActivity.java**

This file contains the logic behind the blacklist section accessible from the burger menu.

**ProfileActivity.java**

This file contains the logic behind the profile activity accessible from the burger menu.